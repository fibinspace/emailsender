import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
#porty 25, 465, 587
server = smtplib.SMTP('smtp.gmail.com', 587)

server.ehlo()

with open('password.txt', 'r') as f:
    password = f.read()

server.login('yourMailHere@gmail.com', password)

msg = MIMEMultipart()
msg['From'] = 'Grzechu'
msg['To'] = 'receiver@receiver.com'
msg['Subject'] = 'To tylko test skryptu od jednego typa'

with open('message.txt', 'r') as f:
    message = f.read()

msg.attach(MIMEText(message, 'plain'))

filename = 'rail.jpg'
attachment = open(filename, 'br')

p = MIMEBase('application', 'octet-stream')
p.set_payload(attachment.read())

encoders.encode_base64(p)
p.add_header('Content-Disposition', f'attachment; filename={filename}')
msg.attach(p)

text = msg.as_string()

server.sendmail('yourMailHere@gmail.com', 'receiver@receiver.com', text)